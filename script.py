from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
import json

# add your cnr number
CNR_NUMBER = "" 

def get_data():
    # opens chrome
    driver = webdriver.Chrome()
    driver.minimize_window()
    
    # opens the url
    driver.get("https://bombayhighcourt.nic.in/index.php")

    # find Case Status Button and hover on it
    case_status_element = driver.find_element(By.LINK_TEXT, "Case Status")
    actions = ActionChains(driver)
    actions.move_to_element(case_status_element).perform()

    # find `CNR/CIN No. Wise` Button and click on it
    cnr_element = driver.find_element(By.LINK_TEXT, "CNR/CIN No. Wise")
    cnr_element.click()

    # wait until page loads (if not loaded under 10 second then throw exception)
    # once loaded, get captcha url and split captcha value
    wait = WebDriverWait(driver, 10)
    captcha_img_element = wait.until(EC.visibility_of_element_located((By.ID, "captchaimg")))
    src_url = captcha_img_element.get_attribute("src")
    captcha_text = src_url.split("?rand=")[1]

    # find captcha input and enter extracted captcha
    captcha_input_element = driver.find_element(By.ID, "captcha_code")
    captcha_input_element.clear()
    captcha_input_element.send_keys(captcha_text)

    # find cnrno field and enter case/CNR number
    cnr_input_element = driver.find_element(By.NAME, "cnrno")
    cnr_input_element.clear()
    cnr_input_element.send_keys(CNR_NUMBER)

    # click submit
    search_btn = driver.find_element(By.ID, "submit1")
    search_btn.click()

    # wait for the response page and get its table html and convert into json
    table_element = wait.until(EC.presence_of_element_located((By.XPATH, "//table[@style='border:black solid 1px']")))
    table_html = table_element.get_attribute("outerHTML")

    soup = BeautifulSoup(table_html, "html.parser")

    # Extract table data and create a JSON structure
    table_data = []
    for row in soup.find_all("tr"):
        row_data = [cell.get_text(strip=True) for cell in row.find_all("td")]

        if len(row_data) >= 2:
            table_data.append({
                row_data[0].replace(":-", "").rstrip(): row_data[1]
            })
        else:
            table_data.append(row_data)

    # remove blank arrays
    filtered_list = list(filter(lambda arr: arr != [] and arr != [''], table_data))

    # save json as file
    file_path="response.json"
    with open(file_path, 'w') as file:
        json.dump(filtered_list, file, indent=4)
        
    # save screenshot
    driver.save_screenshot("response.png")
    driver.quit()
    
    
# need to update
def convert_to_object(item):
    if isinstance(item, list) and len(item) == 1 and ":-" in item[0]:
        key, value = item[0].split(":-", 1)
        return {key.strip(): value.strip()}
    else:
        return item
    
    
    
get_data()